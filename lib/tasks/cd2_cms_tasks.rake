namespace :cms do
  task install: :environment do

    Cd2Cms::User.find_or_create_by(email: 'web@cd2solutions.co.uk') do |user|
      user.name = 'CD2'
      user.email = 'web@cd2solutions.co.uk'
      user.password = 'pass123'
      user.cd2admin = true
    end

    site = Cd2Cms::SettingGroup.find_or_create_by(name: 'Site', body: 'Change information about your store')
    Cd2Cms::Setting.new_setting(:site_name, default: 'Website', group: :site) unless Cd2Cms::Setting.key_exists? :site_name
    Cd2Cms::Setting.new_setting(:enquiry_email, default: 'site@example.com', group: :site) unless Cd2Cms::Setting.key_exists? :enquiry_email

  end
end
