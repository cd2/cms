module Cd2Cms
  class Engine < ::Rails::Engine
    isolate_namespace Cd2Cms

    config.generators do |g|
      g.test_framework :rspec, :fixture => false
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
      g.assets false
      g.helper false
    end

    initializer :append_migrations do |app|
      unless app.root.to_s.match root.to_s
        config.paths["db/migrate"].expanded.each do |expanded_path|
          app.config.paths["db/migrate"] << expanded_path
        end
      end
    end

    config.to_prepare do
      Dir.glob(Rails.root + "app/decorators/**/*_decorator*.rb").each do |c|
        require_dependency(c)
      end
    end

    #change the field with errors behaviour
    ActionView::Base.field_error_proc = Proc.new do |html_tag, instance|
      class_attr_index = html_tag.index 'class="'

      if class_attr_index
        html_tag.insert class_attr_index+7, 'error '
      else
        html_tag.insert html_tag.index('>'), ' class="error"'
      end
    end

    initializer "cms.initialize_ckeditor" do |app|
      Ckeditor.setup do |config|
        config.cdn_url = "//cdn.ckeditor.com/4.5.10/standard/ckeditor.js"
      end
      Rails.application.config.assets.precompile += %w( ckeditor/filebrowser/images/gal_del.png )
    end


  end
end
