class AddIsTeaserToCd2CmsImages < ActiveRecord::Migration[5.0]
  def change
    add_column :cd2_cms_images, :teaser_image, :boolean, default: false
  end
end
