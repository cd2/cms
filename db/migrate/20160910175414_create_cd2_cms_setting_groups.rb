class CreateCd2CmsSettingGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :cd2_cms_setting_groups do |t|
      t.string :name
      t.string :machine_name
      t.text :body

      t.timestamps
    end
  end
end
