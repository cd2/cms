class CreateCd2CmsWeights < ActiveRecord::Migration[5.0]
  def change
    create_table :cd2_cms_weights do |t|

      t.integer :value
      t.references :orderable, polymorphic: true

      t.timestamps
    end
  end
end
