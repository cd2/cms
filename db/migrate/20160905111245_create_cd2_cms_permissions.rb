class CreateCd2CmsPermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :cd2_cms_permissions do |t|
      
      t.references :role
      t.references :permission_subject
      
      t.boolean :read
      t.boolean :new
      t.boolean :edit
      t.boolean :remove

      t.timestamps
    end
  end
end
