class CreateCd2CmsPermissionSubjects < ActiveRecord::Migration[5.0]
  def change
    create_table :cd2_cms_permission_subjects do |t|


      t.string :name
      t.text :description

      t.string :subject_type
      t.integer :subject_id

      t.timestamps
    end
  end
end
