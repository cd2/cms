# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161102202146) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cd2_cms_blogs", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "teaser_body"
  end

  create_table "cd2_cms_enquiries", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cd2_cms_images", force: :cascade do |t|
    t.string   "image"
    t.string   "alt"
    t.string   "caption"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.boolean  "featured_image", default: false
    t.integer  "order"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "teaser_image",   default: false
  end

  create_table "cd2_cms_page_infos", force: :cascade do |t|
    t.string   "title"
    t.text     "meta_description"
    t.string   "url_alias"
    t.boolean  "published",        default: true
    t.boolean  "protected",        default: false
    t.integer  "page_id"
    t.string   "page_type"
    t.integer  "author_id"
    t.boolean  "home_page",        default: false
    t.integer  "order"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["page_type", "page_id"], name: "index_cd2_cms_page_infos_on_page_type_and_page_id", unique: true, using: :btree
    t.index ["page_type", "url_alias"], name: "index_cd2_cms_page_infos_on_page_type_and_url_alias", unique: true, using: :btree
  end

  create_table "cd2_cms_pages", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.string   "layout"
    t.boolean  "in_menu"
    t.string   "menu_item"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cd2_cms_permission_subjects", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "subject_type"
    t.integer  "subject_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "cd2_cms_permissions", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "permission_subject_id"
    t.boolean  "read"
    t.boolean  "new"
    t.boolean  "edit"
    t.boolean  "remove"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["permission_subject_id"], name: "index_cd2_cms_permissions_on_permission_subject_id", using: :btree
    t.index ["role_id"], name: "index_cd2_cms_permissions_on_role_id", using: :btree
  end

  create_table "cd2_cms_projects", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "teaser_body"
  end

  create_table "cd2_cms_roles", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "cd2_cms_service_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id",   null: false
    t.integer "descendant_id", null: false
    t.integer "generations",   null: false
    t.index ["ancestor_id", "descendant_id", "generations"], name: "service_anc_desc_idx", unique: true, using: :btree
    t.index ["descendant_id"], name: "service_desc_idx", using: :btree
  end

  create_table "cd2_cms_service_projects", force: :cascade do |t|
    t.integer  "service_id"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id", "service_id"], name: "index_cd2_cms_service_projects_on_project_id_and_service_id", unique: true, using: :btree
  end

  create_table "cd2_cms_services", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.integer  "parent_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "teaser_body"
    t.index ["parent_id"], name: "index_cd2_cms_services_on_parent_id", using: :btree
  end

  create_table "cd2_cms_setting_groups", force: :cascade do |t|
    t.string   "name"
    t.string   "machine_name"
    t.text     "body"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "cd2_cms_setting_type_images", force: :cascade do |t|
    t.string   "value"
    t.string   "default"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cd2_cms_setting_type_strings", force: :cascade do |t|
    t.string   "value"
    t.string   "default"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cd2_cms_setting_type_texts", force: :cascade do |t|
    t.string   "value"
    t.string   "default"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cd2_cms_settings", force: :cascade do |t|
    t.string   "key"
    t.string   "data_type"
    t.integer  "data_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "setting_group_id"
    t.index ["data_id", "data_type"], name: "index_cd2_cms_settings_on_data_id_and_data_type", unique: true, using: :btree
    t.index ["data_type", "data_id"], name: "index_cd2_cms_settings_on_data_type_and_data_id", unique: true, using: :btree
    t.index ["key"], name: "index_cd2_cms_settings_on_key", unique: true, using: :btree
    t.index ["setting_group_id"], name: "index_cd2_cms_settings_on_setting_group_id", using: :btree
  end

  create_table "cd2_cms_teasers", force: :cascade do |t|
    t.string   "image"
    t.string   "heading"
    t.text     "body"
    t.string   "teaseable_type"
    t.integer  "teaseable_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["teaseable_type", "teaseable_id"], name: "index_cd2_cms_teasers_on_teaseable_type_and_teaseable_id", using: :btree
  end

  create_table "cd2_cms_testimonials", force: :cascade do |t|
    t.text     "quote"
    t.string   "author"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_cd2_cms_testimonials_on_project_id", using: :btree
  end

  create_table "cd2_cms_user_roles", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_cd2_cms_user_roles_on_role_id", using: :btree
    t.index ["user_id"], name: "index_cd2_cms_user_roles_on_user_id", using: :btree
  end

  create_table "cd2_cms_users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "name"
    t.boolean  "cd2admin",               default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["email"], name: "index_cd2_cms_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_cd2_cms_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "cd2_cms_weights", force: :cascade do |t|
    t.integer  "value"
    t.string   "orderable_type"
    t.integer  "orderable_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["orderable_type", "orderable_id"], name: "index_cd2_cms_weights_on_orderable_type_and_orderable_id", using: :btree
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
    t.index ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  end

end
