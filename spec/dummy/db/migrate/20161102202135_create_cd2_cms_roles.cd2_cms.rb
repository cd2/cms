# This migration comes from cd2_cms (originally 20160905110535)
class CreateCd2CmsRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :cd2_cms_roles do |t|

      t.string :name
      t.text   :description
      
      t.timestamps
    end
  end
end
