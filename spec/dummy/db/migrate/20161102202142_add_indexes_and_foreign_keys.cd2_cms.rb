# This migration comes from cd2_cms (originally 20160911143616)
class AddIndexesAndForeignKeys < ActiveRecord::Migration[5.0]
  def change

    add_index :cd2_cms_page_infos, [:page_type, :url_alias], unique: true
    add_index :cd2_cms_page_infos, [:page_type, :page_id], unique: true
    
    add_index :cd2_cms_service_projects, [:project_id, :service_id], unique: true

    add_index :cd2_cms_services, [:parent_id]
    
    add_index :cd2_cms_settings, [:data_type, :data_id], unique: true
    
    add_index :cd2_cms_testimonials, [:project_id]


  end
end
