# This migration comes from cd2_cms (originally 20160910175816)
class ModifySettings < ActiveRecord::Migration[5.0]
  def change
    add_reference :cd2_cms_settings, :setting_group, index: true

    add_index :cd2_cms_settings, :key, unique: true
    add_index :cd2_cms_settings, [:data_id, :data_type], unique: true
  end
end
