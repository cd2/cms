# This migration comes from cd2_cms (originally 20160912084637)
class AddTeaserBodyToStuff < ActiveRecord::Migration[5.0]
  def change

    add_column :cd2_cms_services, :teaser_body, :text
    add_column :cd2_cms_projects, :teaser_body, :text
    add_column :cd2_cms_blogs, :teaser_body, :text

  end
end
