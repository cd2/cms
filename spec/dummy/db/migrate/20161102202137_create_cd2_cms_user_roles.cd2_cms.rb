# This migration comes from cd2_cms (originally 20160905113214)
class CreateCd2CmsUserRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :cd2_cms_user_roles do |t|
      t.references :user
      t.references :role

      t.timestamps
    end
  end
end
