# This migration comes from cd2_cms (originally 20160912084429)
class AddIsTeaserToCd2CmsImages < ActiveRecord::Migration[5.0]
  def change
    add_column :cd2_cms_images, :teaser_image, :boolean, default: false
  end
end
