Rails.application.routes.draw do

  mount Cd2Cms::Engine => "/admin"
  scope module: :cd2_cms do
    get '/sitemap', defaults: { :format => :xml }, to: 'sitemap#show'
  end

  root 'pages#home'
end
