task reset_all: :environment do

  Rake::Task['db:drop'].invoke

  if Rails.env.development?
    Dir.foreach(Rails.root + 'db/migrate') do |f|
      filename = File.join(Rails.root + 'db/migrate', f)
      next if f == '.' || f == '..'
      File.delete(filename) if filename.ends_with? '.cd2_cms.rb'
    end
    Rake::Task['cd2_cms:install:migrations'].invoke
  end

  Rake::Task['db:create'].invoke
  Rake::Task['db:migrate'].invoke
  Rake::Task['cms:install'].invoke

  Cd2Cms::Page.create!(
      name: 'Home',
      body: 'This is the home page',
      layout: 'home'
  ).page_info.update(
      url_alias: 'home',
      home_page: true,
      protected: true
  )

  Cd2Cms::User.create!(
      name: 'John Doe',
      email: 'fake@email.co',
      password: 'pass123',
  )

  10.times do |i|
    puts "#{i+1}/10 Enquiries"
    Cd2Cms::Enquiry.create!(
        name: Faker::Name.name,
        email: Faker::Internet.email,
        body: Faker::Hipster.paragraphs(1+rand(2)).join('\n')
    )
  end

end