$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "cd2_cms/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "cd2_cms"
  s.version     = Cd2Cms::VERSION
  s.authors     = ["Shane"]
  s.email       = ["shane@cd2solutions.co.uk"]
  s.summary     = "Summary of Cd2Cms."
  s.description = "Description of Cd2Cms."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.required_ruby_version = '>= 2.3.0'

  s.add_dependency "rails", "~> 5.0.0"

  s.add_dependency 'jquery-rails'
  s.add_dependency 'jquery-ui-rails'

  s.add_dependency 'coffee-rails'
  s.add_dependency 'haml-rails'
  s.add_dependency 'sass-rails', '~> 5.0'

  s.add_dependency 'font-awesome-rails'
  s.add_dependency "devise"
  s.add_dependency "paper_trail"
  s.add_dependency 'sweetalert-rails'
  s.add_dependency 'ransack'
  s.add_dependency 'select2-rails'

  s.add_dependency 'carrierwave'
  s.add_dependency 'mini_magick'
  s.add_dependency 'fog'

  s.add_dependency 'closure_tree'

  s.add_dependency 'ckeditor'


  s.add_dependency 'cancancan'
  s.add_development_dependency "pg"


end
