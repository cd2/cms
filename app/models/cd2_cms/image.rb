require_dependency 'cd2_cms/concerns/orderable'

module Cd2Cms
  class Image < ApplicationRecord
    include Orderable

    default_scope { order(order: :asc) }

    self.permitted_params = [:alt, :caption, :image, :image_cache, :feature_image, :teaser_image, :weight, :featured_image]

    validates :image, presence: true

    belongs_to :imageable, polymorphic: true
    mount_uploader :image, ImageUploader


  end
end
