module Cd2Cms
  class Ability
    include CanCan::Ability

    def initialize(user)

      user ||= Cd2Cms::User.new

      if user.cd2admin?
        can :manage, :all
      end

      user.permissions.each do |permission|
        opts = {}
        opts[:id] = permission.subject_id if permission.subject_id

        can [:read],                      permission.subject, opts if permission.read?
        can [:new, :create],              permission.subject, opts if permission.new?
        can [:edit, :update],             permission.subject, opts if permission.edit?
        can [:destroy, :confirm_destroy], permission.subject, opts if permission.remove?

      end


    end
  end
end
