require_dependency 'cd2_cms/concerns/site_page'
require_dependency 'cd2_cms/concerns/imageable'
require_dependency 'cd2_cms/concerns/orderable'

module Cd2Cms
  class Page < ApplicationRecord
    include SitePage
    include Imageable
    include Orderable

    self.permitted_params = [:name, :body, :layout, :in_menu, :menu_item]
    
    validates :name, presence: true
    
    scope :menu_pages, -> { where(in_menu: true) }

    delegate :home_page, to: :page_info
    delegate :home_page?, to: :page_info

    



  end
end
