module Cd2Cms
  class Setting < ApplicationRecord

    self.permitted_params = [:value]

    belongs_to :data, polymorphic: true, autosave: true
    belongs_to :group, class_name: 'Cd2Cms::SettingGroup', foreign_key: :setting_group_id

    validates :data, presence: true
    validates :group, presence: true
    validates :key, presence: true, uniqueness: true
    validates :value, presence: true, if: :data

    delegate :value, to: :data
    delegate :value=, to: :data

    # delegate any method calls to the data models (setting types)
    def method_missing name, *args
      if data.respond_to? name
        data.send(name, *args)
      else
        super
      end
    end

    def self.new_setting key, *args
      opts = args.extract_options!
      value = opts.fetch(:value, args[0])
      default = opts.fetch(:default, value || args[0])
      group_name = opts.fetch(:group, :site).to_s.parameterize
      type = opts.fetch(:type, value.class.name.downcase).to_sym

      setting_type = case type
         when :image
           SettingType::Image
         when :text
           SettingType::Text
         else
           SettingType::String
       end

      data = setting_type.new(value: value, default: default)
      group = SettingGroup.find_by!(machine_name: group_name)

      group.settings.create!(key: key, data: data)
    end

    def self.get key
      setting = find_by(key: key)
      raise KeyNotFound.new(key) unless setting
      setting.data.value
    end

    def self.set key, value
      setting = find_by(key: key)
      raise KeyNotFound.new(key) unless setting
      setting.data.update(value: value)
    end

    def self.key_exists? key
      find_by(key: key).present?
    end

    class KeyNotFound < StandardError
      def initialise key
        super "#{key} not found"
      end
    end

  end
end
