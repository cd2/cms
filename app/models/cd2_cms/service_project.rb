module Cd2Cms
  class ServiceProject < ApplicationRecord
    belongs_to :service
    belongs_to :project
  end
end
