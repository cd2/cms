module Cd2Cms
  module SettingType
    extend ActiveSupport::Concern

    def self.table_name_prefix
      'cd2_cms_setting_type_'
    end


    included do

      has_one :setting, as: :data

      def value
        self.value_present? ? super : self.default
      end

      def value_present?
        self[:value].present?
      end


    end


  end
end
