module Cd2Cms
  class SettingType::Image < ApplicationRecord
    include SettingType

    mount_uploader :default, ImageUploader
    mount_uploader :value, ImageUploader

    def form_helper
      :file_field
    end

    def type
      if setting.value.respond_to? :file
        :image
      else
        :string
      end
    end

    def default
      if super.file.present?
        super
      else
        default_string
        super
      end
    end

    def default= val
      self.default_string = val
      super
    end

    def value_present?
      value.file.present?
    end

  end
end
