module Cd2Cms
  class SettingType::Text < ApplicationRecord
    include SettingType


    def form_helper
      :text_area
    end

    def type
      :text
    end

  end
end
