module Cd2Cms
  class SettingType::String < ApplicationRecord
    include SettingType


    def form_helper
      :text_field
    end

    def type
      :string
    end

  end
end
