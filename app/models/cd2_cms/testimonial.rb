require_dependency 'cd2_cms/concerns/orderable'

module Cd2Cms
  class Testimonial < ApplicationRecord
    include Orderable

    self.permitted_params = [:quote, :author, :project_id]
    validates :quote, :author, presence: true
    belongs_to :project, optional: true

  end
end
