module Cd2Cms
  class Permission < ApplicationRecord
    belongs_to :role
    belongs_to :permission_subject


    delegate :name, :description, :subject, :subject_id, to: :permission_subject


  end
end
