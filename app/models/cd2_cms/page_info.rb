module Cd2Cms
  class PageInfo < ApplicationRecord

    self.permitted_params = [:title, :meta_description, :url_alias, :published, :protected, :home_page]

    belongs_to :page, polymorphic: true
    belongs_to :author, class_name: 'User'

    validates :url_alias, presence: true, uniqueness: {scope: :page_type}

    after_create do
      update(url_alias: url_alias)
    end

    before_validation do
      if url_alias.blank?
        self.url_alias = page&.name&.downcase || page.id
      end
    end

    #ensure url_alias is url safe
    def url_alias= val
      super val.to_s.parameterize
    end

    def to_param
      self.url_alias
    end

  end
end
