require_dependency 'cd2_cms/concerns/site_page'
require_dependency 'cd2_cms/concerns/imageable'
require_dependency 'cd2_cms/concerns/teaseable'

module Cd2Cms
  class Blog < ApplicationRecord
    include SitePage
    include Imageable
    include Teaseable
        
    self.permitted_params = [:name, :body]

    validates :name, presence: true

  end
end
