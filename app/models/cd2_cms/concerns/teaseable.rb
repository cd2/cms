module Cd2Cms
  module Teaseable
    extend ActiveSupport::Concern

    included do

      #legacy to prevent old sites breaking
      has_one :teaser, as: :teaseable

      self.permitted_params = [:teaser_body]

      def teaser_body
        if super.present?
          return super
        elsif body.present?
          return ActionController::Base.helpers.strip_tags(body.gsub(/<h(\d)>.*?<\/h(\1)>/, '')).truncate(150, strip_tags: true)
        else
          ''
        end
      end

      #called in views for teaser text and image
      def teaser_body_check
        if teaser_body.nil?
          body
        else
          teaser_body
        end
      end

      def teaser_image_check
        if teaser_image&.url.present?
          teaser_image&.teaser
        else
          image.image
        end
      end

    end

  end
end
