module Cd2Cms
  module SitePage
    extend ActiveSupport::Concern

    included do

      scope :published, -> { includes(:page_info).where("cd2_cms_page_infos.published": true) }

      has_one :page_info, as: :page, autosave: true, dependent: :destroy
      accepts_nested_attributes_for :page_info, update_only: true

      self.permitted_params = [:published, {page_info_attributes: [:id, Cd2Cms::PageInfo]}]

      delegate :url_alias=, to: :page_info
      delegate :title=, to: :page_info
      delegate :published=, to: :page_info

      delegate :protected, to: :page_info
      delegate :published, to: :page_info
      delegate :order, to: :page_info
      delegate :title, to: :page_info
      delegate :meta_description, to: :page_info
      delegate :url_alias, to: :page_info
      delegate :author, to: :page_info

      before_create do
        create_page_info unless page_info
      end

      def to_param
        page_info.to_param
      end

      def page_info
        super || build_page_info(page: self)
      end

      def assign_author user
        page_info.author_id = user.id
        page_info.save
      end

    end

    class_methods do

      def get_from_url url=nil
        cond = url ? {url_alias: url} : {home_page: true}
        all.joins(:page_info).where(cd2_cms_page_infos: cond).first!
      end

      def root_page
        PageInfo.find_by(page_type: page_type, root: true).page
      end

    end

  end
end
