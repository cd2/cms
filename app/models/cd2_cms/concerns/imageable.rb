module Cd2Cms
  module Imageable
    extend ActiveSupport::Concern

    included do

      has_many :images, as: :imageable, autosave: true, dependent: :destroy
      has_one :feature_image, ->  { where(featured_image: true) }, as: :imageable, class_name: 'Cd2Cms::Image'
      has_one :teaser_image, ->  { where(teaser_image: true) }, as: :imageable, class_name: 'Cd2Cms::Image'

      accepts_nested_attributes_for :images, allow_destroy: true
      self.permitted_params = [new_images: [], images_attributes: [:id, :_destroy, Cd2Cms::Image]]

      def new_images= val
        Array(val).each do |image|
          images.build(image: image)
        end
      end

      def feature_image
        super&.image || images.first&.image
      end

      def feature_image?
        self[:feature_image]&.image&.url.present?
      end

      def teaser_image
        super&.image || images.first&.image
      end

      def teaser_image?
        self[:teaser_image]&.image&.url.present?
      end

    end

  end
end
