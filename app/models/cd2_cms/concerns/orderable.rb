module Cd2Cms
  module Orderable
    extend ActiveSupport::Concern

    included do

      has_one :_weight, dependent: :destroy, class_name: 'Cd2Cms::Weight', as: :orderable
      accepts_nested_attributes_for :_weight

      scope :ordered, -> { includes(:_weight).order('cd2_cms_weights.value asc') }

      def _weight
        super || build__weight
      end

      def weight
        _weight.value
      end

      def weight= (val)
        _weight.value = val
      end

      def self.update_order ordered_ids
        order = ordered_ids.each_with_index.map { |_, i| {weight: i+1} }
        update(ordered_ids, order)
      end

    end

  end
end
