module Cd2Cms
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true
    # has_paper_trail

    def self.permitted_params= *val
      (@permitted_params ||= []).concat val
    end

    def self.permitted_params
      parse_permitted_params (@permitted_params || [])
    end

    private

    def self.parse_permitted_params params
      x = []
      puts "PARAMS: #{params}"
      params.each do |k|
        if params.is_a? Hash
          x << ({ k[0] => self.parse_permitted_params(Array(k[1])) })
        else
          case k
          when Hash, Array
            x.concat self.parse_permitted_params(k)
          else
            if (k.is_a? Class) && k < ActiveRecord::Base
              x.concat (k&.send(:permitted_params) || [])
            else
              x << k
            end
          end
        end
      end
      return x
    end

  end
end
