module Cd2Cms
  class Weight < ApplicationRecord

    belongs_to :orderable, polymorphic: true

    def value
      super || 0
    end

  end
end
