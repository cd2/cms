require_dependency 'cd2_cms/concerns/site_page'
require_dependency 'cd2_cms/concerns/imageable'
require_dependency 'cd2_cms/concerns/teaseable'
require_dependency 'cd2_cms/concerns/orderable'

module Cd2Cms
  class Project < ApplicationRecord
    include SitePage
    include Imageable
    include Teaseable
    include Orderable

    self.permitted_params = [:name, :body, service_ids: []]

    has_one :testimonial

    has_many :service_projects
    has_many :services, through: :service_projects


  end
end
