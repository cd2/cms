module Cd2Cms
  class Enquiry < ApplicationRecord

    self.permitted_params = [:name, :email, :body]

    validates :name, presence: true

    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email,
      presence: true,
      length: {maximum: 255},
      format: {with:  VALID_EMAIL_REGEX}


    after_create_commit :send_email


    private

    def send_email
      Cd2Cms::EnquiriesMailer.new_enquiry(self).deliver_later
    end

  end
end
