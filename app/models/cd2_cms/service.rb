require_dependency 'cd2_cms/concerns/site_page'
require_dependency 'cd2_cms/concerns/imageable'
require_dependency 'cd2_cms/concerns/teaseable'
require_dependency 'cd2_cms/concerns/orderable'

module Cd2Cms
  class Service < ApplicationRecord
    has_closure_tree
    
    include SitePage
    include Imageable
    include Teaseable
    include Orderable

    self.permitted_params = [:name, :body, :parent_id]

    validates :name, presence: true

    scope :roots, -> { where(parent_id: nil) }

    belongs_to :parent, class_name: 'Service'
    has_many :children, class_name: 'Service', foreign_key: :parent_id

    has_many :service_projects
    has_many :projects, through: :service_projects

  end
end
