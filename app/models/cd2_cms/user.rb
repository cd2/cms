module Cd2Cms
  class User < ApplicationRecord
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable,
           :recoverable, :rememberable, :trackable, :validatable

    has_many :user_roles
    has_many :roles, through: :user_roles
    has_many :permissions, -> { distinct }, through: :roles

    validates :name, :presence => true

    self.permitted_params = [:name, :email, :password, :password_confirmation]
  end
end
