$(window).on 'page:load load', ->

  $('a[data-delete-link]').on 'click', (e) ->
    url = $(this).data('delete-link')

    swal {
      title: 'Are you sure?'
      text: 'This action cannot be undone!'
      type: 'warning'
      showCancelButton: true
      confirmButtonColor: '#DD6B55'
      confirmButtonText: 'Yes, delete it!'
      closeOnConfirm: false
      showLoaderOnConfirm: true
    }, ->
      $.ajax
        url: url
        method: 'POST'
        data: _method: 'DELETE'
        success: ->
          swal {
            title: 'Deleted!'
            text: 'Its been deleted.'
            type: 'success'
          }, ->
            location.reload()
        error: ->
          swal
            title: 'Error!'
            text: 'Sorry a problem has occurred'
            type: 'error'

    e.preventDefault()
