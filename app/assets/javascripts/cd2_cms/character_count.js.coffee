$ ->
  $('[data-length-min]')
    .each ->
      $wrapper = $('<div data-length-count-wrapper class="length_count_wrapper">')
      $wrapper.css('position', 'relative')
      $(this).wrap($wrapper)

      $count = $('<div data-length-count/>')
      $count.insertAfter(this)
      $(this).data('length-obj', $count)
      console.log($wrapper, $count)

      console.log(this)
    .on 'keyup', ->
      len = this.value.length
      min = $(this).data('length-min') || 0
      max = $(this).data('length-max') || Number::Infinity

      $count = $(this).data('length-obj')
      txt = "#{len}"

      $count.text(txt)
      if min > this.value.length
        $count.addClass('length_too_short')
      else
        $count.removeClass('length_too_short')

      if this.value.length > max
        $count.addClass('length_too_long')
      else
        $count.removeClass('length_too_long')
    .trigger 'keyup'
