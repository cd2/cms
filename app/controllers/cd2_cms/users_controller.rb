require_dependency "cd2_cms/application_controller"

module Cd2Cms
  class UsersController < ApplicationController

    def update
      if @object.update(object_params)
        flash[:success] = "User updated"
        redirect_to users_path
      else
        render :edit
      end
    end

    def destroy
      if @object != current_user && @object.destroy
        flash[:success] = 'User deleted.'
      else
        flash[:error] = 'An error occurred when deleting that user.'
      end
      redirect_to users_path
    end
    
    private

    def object_params
      if params[:user][:password].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
      end
      params.require(:user).permit(:name, :email, :password)
    end

    def set_resource_driver
      @resource_driver = Cd2Cms::User
    end

    def set_object
      @object = @resource_driver.find params[:id]
    end

    def set_links
      @index_path = users_path
      @new_path = new_user_path
    end

  end
end
