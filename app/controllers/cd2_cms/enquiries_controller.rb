require_dependency "cd2_cms/application_controller"

module Cd2Cms
  class EnquiriesController < ApplicationController

    private

    def set_resource_driver
      @resource_driver = Cd2Cms::Enquiry
    end

    def set_object
      @object = @resource_driver.find params[:id]
    end

    def set_links
      @index_path = enquiries_path
    end

  end
end
