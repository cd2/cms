require_dependency 'cd2_cms/concerns/show_page'

module Cd2Cms
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    #authorize_resource
    #check_authorization :unless => :devise_controller?
    before_action :authenticate_user!

    before_action unless: :devise_controller? do
      set_resource_driver
      set_paper_trail_whodunnit
      set_ransack_search_param
      set_links
    end
    
    before_action :set_object, only: [:show, :edit, :update, :destroy], unless: :devise_controller?

    layout :layout

    if Rails.env.development?
      before_action do
        if ENV['USER'] == 'henry' && !signed_in?
          sign_in(:user, Cd2Cms::User.first)
        end
      end
    end

    def logs
      PaperTrail::Version.all
    end

    def index
      if @resource_driver.respond_to? :ordered
        @q = @resource_driver.try(:ordered).ransack(params[:q])
      else
        @q = @resource_driver.ransack(params[:q])
      end
      @objects = @q.result(distinct: true)
    end

    def show
    end

    def new
      @object = @resource_driver.new
    end

    def create
      @object = @resource_driver.new(object_params)
      if (params[:commit] == 'upload')
        render :new
      elsif @object.save
        @object.assign_author(current_user) if @object.class.included_modules.include? Cd2Cms::SitePage
        flash[:notice] = "#{controller_name.classify} was created"
        redirect_to @index_path
      else
        render :new
      end
    end

    def edit
    end

    def update
      if (params[:commit] == 'upload')
        @object.assign_attributes(object_params)
        render :edit
      elsif @object.update(object_params)
        flash[:notice] = "The #{controller_name.classify} was updated"
        redirect_to @index_path
      else
        render :edit
      end
    end

    def destroy
      @object.destroy
      flash[:success] = "#{controller_name.classify} was destroyed"
      redirect_to @index_path
    end

    def sort
      @resource_driver.update_order(params[@resource_driver.model_name.param_key])
      respond_to do |format|
        format.html { redirect_to @index_path if @index_path }
        format.js { head :ok, content_type: "text/html" }
      end
    end

    def confirm_destroy      
    end

    private

      def set_ransack_search_param
        @search_param = 'name'
      end

      def object_params
        params.require(@resource_driver.model_name.param_key).permit(@resource_driver.permitted_params)
      end

      def layout
        devise_controller? ? "cd2_cms/devise_application" : "cd2_cms/application"
      end

      def current_ability
        @current_ability ||= Ability.new(current_user)
      end

  end
end
