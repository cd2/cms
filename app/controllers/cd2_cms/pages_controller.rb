require_dependency "cd2_cms/application_controller"

module Cd2Cms
  class PagesController < ApplicationController

    def index
      @q = @resource_driver.ordered.ransack(params[:q])
      @objects = @q.result(distinct: true)
    end

    private

      def set_resource_driver
        @resource_driver = Cd2Cms::Page
      end

      def new_image_params
        params.require(:page).permit(new_images: [], images_attributes: [:id, :_destroy, Cd2Cms::Image])
      end

      def set_object
        @object = @resource_driver.get_from_url params[:id]
      end

      def set_links
        @index_path = pages_path
        @new_path = new_page_path
        @sortable = sort_pages_path
      end

  end
end
