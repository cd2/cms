require_dependency "cd2_cms/application_controller"

module Cd2Cms
  class SitemapController < ActionController::Base
    protect_from_forgery with: :exception

    def show
    end

  end
end
