require_dependency "cd2_cms/application_controller"

module Cd2Cms
  class ServicesController < ApplicationController

    def update
      if (params[:commit] == 'upload')
        @object.assign_attributes(new_image_params)
        render :edit
      else
        super
      end
    end

    private

    def set_resource_driver
      @resource_driver = Cd2Cms::Service
    end

    def new_image_params
      params.require(:service).permit(new_images: [], images_attributes: [:id, :_destroy, *Cd2Cms::Image.permitted_params])
    end

    def set_object
      @object = @resource_driver.get_from_url params[:id]
    end

    def set_links
      @index_path = services_path
      @new_path = new_service_path
      @sortable = sort_services_path
    end

  end
end
