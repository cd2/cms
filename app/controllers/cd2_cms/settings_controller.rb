require_dependency "cd2_cms/application_controller"

module Cd2Cms
  class SettingsController < ApplicationController

    def destroy
      @object.update value: nil
      flash[:success] = "Setting was restored to its default"
      redirect_to @index_path
    end

    private

      def set_resource_driver
        @resource_driver = Cd2Cms::Setting
      end

      def set_object
        @object = @resource_driver.find params[:id]
      end

      def set_links
        @index_path = settings_path
      end

      def set_ransack_search_param
        @search_param = 'key'
      end

  end
end
