require_dependency "cd2_cms/application_controller"

module Cd2Cms
  class ProjectsController < ApplicationController

    private

      def set_resource_driver
        @resource_driver = Cd2Cms::Project
      end

      def set_object
        @object = @resource_driver.get_from_url params[:id]
      end

      def set_links
        @index_path = projects_path
        @new_path = new_project_path
        @sortable = sort_projects_path
      end

  end
end
