require_dependency "cd2_cms/application_controller"

module Cd2Cms
  class TestimonialsController < ApplicationController

    private

    def set_resource_driver
      @resource_driver = Cd2Cms::Testimonial
    end

    def set_object
      @object = @resource_driver.find params[:id]
    end

    def set_links
      @index_path = testimonials_path
      @new_path = new_testimonial_path
      @sortable = sort_testimonials_path
    end

    def set_ransack_search_param
      @search_param = 'author'
    end


  end
end
