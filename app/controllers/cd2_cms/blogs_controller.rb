require_dependency "cd2_cms/application_controller"

module Cd2Cms
  class BlogsController < ApplicationController

    private

    def set_resource_driver
      @resource_driver = Cd2Cms::Blog
    end

    def set_object
      @object = @resource_driver.get_from_url params[:id]
    end

    def set_links
      @index_path = blogs_path
      @new_path = new_blog_path
      @sortable = sort_blogs_path
    end


  end
end
