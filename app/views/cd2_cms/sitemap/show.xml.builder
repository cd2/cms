xml.instruct!
xml.urlset(
    'xmlns'.to_sym => "http://www.sitemaps.org/schemas/sitemap/0.9",
    'xmlns:image'.to_sym => "http://www.google.com/schemas/sitemap-image/1.1"
) do

  xml.url do
    xml.loc main_app.root_url
    xml.changefreq("weekly")
  end
  
  Cd2Cms::Page.published.includes(:page_info).where('cd2_cms_page_infos.home_page': false).find_each do |page|
    xml.url do
      xml.loc "#{main_app.root_url}#{page.url_alias}"
      xml.changefreq("weekly")
    end
  end


  xml << (render 'service_sitemap')
  xml << (render 'blog_sitemap')
  xml << (render 'project_sitemap')
  xml << (render 'testimonial_sitemap')
  xml << (render 'additional_sitemap')

end
