if main_app.respond_to? :project_url
  if Cd2Cms::Project.all.any?
    Cd2Cms::Project.published.find_each do |project|
      xml.url do
        xml.loc "#{main_app.project_url(project)}"
        xml.changefreq("weekly")
      end
    end
  end
end
