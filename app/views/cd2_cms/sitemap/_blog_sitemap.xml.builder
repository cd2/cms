if main_app.respond_to? :blog_url
  if Cd2Cms::Blog.all.any?
    Cd2Cms::Blog.published.find_each do |blog|
      xml.url do
        xml.loc "#{main_app.blog_url(blog)}"
        xml.changefreq("weekly")
      end
    end
  end
end
