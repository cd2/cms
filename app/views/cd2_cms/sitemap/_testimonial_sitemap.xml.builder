if main_app.respond_to? :testimonial_url
  if Cd2Cms::Testimonial.all.any?
    Cd2Cms::Testimonial.published.find_each do |testimonial|
      xml.url do
        xml.loc "#{main_app.testimonial_url(testimonial)}"
        xml.changefreq("weekly")
      end
    end
  end
end
