if main_app.respond_to? :service_url
  if Cd2Cms::Service.all.any?
    Cd2Cms::Service.published.find_each do |service|
      xml.url do
        xml.loc "#{main_app.service_url(service)}"
        xml.changefreq("weekly")
      end
    end
  end
end
