module Cd2Cms
  module PagesHelper

    def get_page model, url=params[:id]
      @page = model.get_from_url(url)
      @page_info = @page.page_info
      if @page_info.home_page && url
        redirect_to root_url, :status => 301
      else
        render @page.layout unless @page.layout.blank?
      end
    end

    def meta_tags
      "#{page_title}#{meta_description}".html_safe
    end

    def page_title
      content_tag(:title, @page_info.title) if @page_info&.title&.present?
    end

    def meta_description
      if @page_info&.meta_description&.present?
        tag :meta, name: :description, content: @page_info.meta_description
      end
    end

  end
end
