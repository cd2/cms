module Cd2Cms
  module TabsHelper

    def tab_controller control_name, checked=nil
      radio_button_tag :tab, control_name, (checked || params[:tab]==control_name.to_s), class: 'tab_controller'
    end
    

    def tab_head control_name, text=nil
      text ||= control_name.to_s.humanize

      label_tag("tab_#{control_name}", text)
    end

    def tab control_name, content=nil, &block
      content = capture(&block) if block
      content_tag :div, content, class: 'tab_panel', data: {tab_panel: control_name}
    end

  end
end
