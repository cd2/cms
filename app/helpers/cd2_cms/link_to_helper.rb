module Cd2Cms
  module LinkToHelper

    def link_to_nil text
      link_to "##{text}#", '#' if Rails.env.development?
    end

    def link_to_read text, href, resource, opts={}
      link_to_can text, href, :read, resource, opts
    end

    def link_to_new text, href, resource, opts={}
      link_to_can text, href, :new, resource, opts
    end

    def link_to_edit text, href, resource, opts={}
      link_to_can text, href, :edit, resource, opts
    end

    def link_to_delete text, resource, opts={}
      opts[:data] ||= {}
      opts[:data].merge!({delete_link: url_for(resource)})
      link_to_can text, [:confirm_destroy, resource], :destroy, resource, opts
    end

    def link_to_can text, href, action, subject, opts={}
      if can? action, subject
        if wrapper = opts.delete(:wrapper)
          content_tag wrapper do
            link_to text, href, opts
          end
        else
          link_to text, href, opts
        end
      end
    end


    def link_to_add_fields name, f, assoc, opts={}
      new_object = f.object.send(assoc).build
      id = new_object.object_id
      fields = f.fields_for(assoc, new_object, child_index: id) do |builder|
        render assoc.to_s.singularize + '_fields', f: builder
      end
      link_to name, '#', data: {fields: fields.gsub('\n', ''), fields_id: id}.merge(opts)
    end


  end
end
