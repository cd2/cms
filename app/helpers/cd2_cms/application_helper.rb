module Cd2Cms
  module ApplicationHelper
    include FontAwesome::Rails::IconHelper
    ActionView::Base.default_form_builder = CFormBuilder

    def is_cd2admin?
      signed_in? && current_user.cd2admin?
    end

    def sidebar_section name=nil, &block
      content = capture(&block)
      if content.present?
        content_tag :div, class: 'sidebar_section' do
          if name
            content_tag(:h4, name) +
                content
          else
            content
          end
        end
      end
    end

  end
end
