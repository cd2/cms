module Cd2Cms
  class EnquiriesMailer < ApplicationMailer

    # Subject can be set in your I18n file at config/locales/en.yml
    # with the following lookup:
    #
    #   en.enquiries_mailer.new_enquiry.subject
    #
    def new_enquiry
      @greeting = "Hi"

      mail to: "to@example.org"
    end
  end
end
