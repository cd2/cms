Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
end

Cd2Cms::Engine.routes.draw do

  devise_for :users, {
    class_name: "Cd2Cms::User",
    module: :devise,
  }

  resources :pages, except: [:show] do
    collection do 
      post :sort
    end
    member do
      get :confirm_destroy
    end
  end

  resources :services, except: [:show] do
    collection do 
      post :sort
    end
    member do
      get :confirm_destroy
    end
  end

  resources :projects, except: [:show] do
    collection do 
      post :sort
    end
    member do
      get :confirm_destroy
    end
  end

  resources :testimonials, except: [:show] do
    collection do
      post :sort
    end
    member do
      get :confirm_destroy
    end
  end

  resources :blogs, except: [:show] do
    collection do
      post :sort
    end
    member do
      get :confirm_destroy
    end
  end

  resources :enquiries, only: [:index, :show, :destroy] do
    member do
      get :confirm_destroy
    end
  end

  resources :users, except: [:show] do
    member do
      get :confirm_destroy
    end
  end

  resources :settings, only: [:index, :edit, :update, :destroy] do
    member do
      get :confirm_destroy
    end
  end

  root 'pages#index'

end
